<?php
    
    if ($_POST['g-recaptcha-response'] == '') {
        echo "Recaptcha no validado";
    } else {
    $obj = new stdClass();
    $obj->secret = "6Lccm2QpAAAAAI3Od_Jof-Ott-gbcK_qcRtrKr-1";
    $obj->response = $_POST['g-recaptcha-response'];
    $obj->remoteip = $_SERVER['REMOTE_ADDR'];
    $url = 'https://www.google.com/recaptcha/api/siteverify';

    $options = array(
        'http' => array(
            'header' => "Content-type: application/x-www-form-urlencoded\r\n",
            'method' => 'POST',
            'content' => http_build_query($obj)
        )
    );
    $context = stream_context_create($options);
    $result = file_get_contents($url, false, $context);

    $validar = json_decode($result);

    if($validar -> success) {
            
        $error = '';
        $cuerpo = '';

        $fechaini=date("Y-m-d");

        if(empty($_POST["name"])){
            $error .= 'Ingrese el nombre </br>';
        }else{
            $name = $_POST["name"];
            $name = filter_var($name, FILTER_SANITIZE_STRING);
        }

        if(empty($_POST["apellido"])){
            $error .= 'Ingresa el apellido </br>';
        }else{
            $apellido = $_POST["apellido"];
            $apellido = filter_var($apellido, FILTER_SANITIZE_STRING);
        }

        if(empty($_POST["message"])){
            $error .= 'Ingresar el Mensaje </br>';
        }else{
            $message = $_POST["message"];
            $message = filter_var($message, FILTER_SANITIZE_STRING);
        }

        if(empty($_POST["phone"])){
            $error .= 'Ingresar Teléfono </br>';
        }else{
            $phone = $_POST["phone"];
            $phone = filter_var($phone, FILTER_SANITIZE_STRING);
        }
    
        if(empty($_POST["email"])){
            $error .= 'Ingrese el E-mail</br>';
        }else{
            $email = $_POST["email"];
            if(!filter_var($email,FILTER_VALIDATE_EMAIL)){
                $error .= 'Ingresa un E-mail verdadero</br>';
            }else{
                $email = filter_var($email,FILTER_SANITIZE_EMAIL);
            }
        } 
        
        if($error == ''){

            $cuerpo.= "Se completo el formulario de la web de JCG Pinturas & Servicios Integrales. Los datos son: ";
            $cuerpo.="\n";
            $cuerpo.="\n";
        
            $cuerpo .= "Nombre: ";
            $cuerpo .= $name;
            $cuerpo .= "\n";
        
            $cuerpo .= "Apellido: ";
            $cuerpo .= $apellido;
            $cuerpo .= "\n";
            
            $cuerpo .= "Email: ";
            $cuerpo .= $email;
            $cuerpo .= "\n";
        
            $cuerpo .= "Telefono: ";
            $cuerpo .= $phone;
            $cuerpo .= "\n";
        
            $cuerpo .= "Mensaje: ";
            $cuerpo .= $message;
            $cuerpo .= "\n";
        
            $cabeceras = 'Reply-To:' .$email . "\r\n" .
                            'Cc:lucianocdcopy@hotmail.com' . "\r\n".
                            'X-Mailer: PHP/' . phpversion();
        
            //DIRECCIÓN
            $enviarA = 'lucianocdcopy@hotmail.com';
            // $enviarA = 'sebastianr@bandofcoders.com';
            $asunto = 'Consulta JCG Pinturas & Servicios Integrales';
        
                //GUARDAR DATOS EN LA BASE DE DATOS - REGISTRO //
            //    require_once('bd_conexion.php');
            //    $sql = "INSERT INTO `brochures_espana` (`id`, `nombre`, `email`, `role`, `company`, `telefono`, `evento`, `info`,  `fecha_solicitud`,  `created_at`,  `updated_at`) ";
            //    $sql .= "VALUES (NULL, '{$nombre}', '{$email}','{$role}', '{$company}', '{$phone}', '{$evento}', '{$about}', '{$fechaini}', '{$fechaini}', '{$fechaini}');";
        
            //    $resultado = $conn->query($sql);
        
            //    $conn->close();

            // ENVIAR CORREO
            $success = mail($enviarA,$asunto,$cuerpo,$cabeceras);

            echo 'exito';
        }else{
            echo $error;
        }
    } else {
        echo "reCaptcha no validado";
    }
}
?>